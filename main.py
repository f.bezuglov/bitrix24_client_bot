import telebot
from telebot import types
import pandas as pd
from funcs import preprocess_text, check_keywords, get_contacts_by_webhook


bot = telebot.TeleBot('')

table = pd.read_csv('https://docs.google.com/spreadsheets/d/' + 
                   '' +
                   '/export?gid=0&format=csv',
                  )
table = table.fillna('')
original_table = table.copy()
for i,row in table.iterrows():
    table.loc[i, 'FAQ'] = preprocess_text(table.loc[i, 'FAQ'], del_punct=True, do_lemmatize=True)
    table.loc[i, 'Ключевое слово 1'] = preprocess_text(table.loc[i, 'Ключевое слово 1'], del_punct=True, do_lemmatize=True)
    table.loc[i, 'Ключевое слово 2'] = preprocess_text(table.loc[i, 'Ключевое слово 2'], del_punct=True, do_lemmatize=True)

faq_list = original_table['FAQ'].values.tolist()
faq_answer_list = original_table['Ответ'].values.tolist()


menu_markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
button1 = types.KeyboardButton(faq_list[0])
button2 = types.KeyboardButton(faq_list[1])
button3 = types.KeyboardButton(faq_list[2])
menu_markup.row(button1, button2, button3)

def send_menu(chat_id):
    bot.send_message(chat_id, "Добро пожаловать! Ответы на популярные вопросы (FAQ) можете узнать, кликная на кнопки", reply_markup=menu_markup)

@bot.message_handler(content_types=['new_chat_members'])
def on_new_chat_member(message):
    send_menu(message.chat.id)

@bot.message_handler(commands=['start'])
def start(message):
    send_menu(message.chat.id)
    print('chat_id: ' + str(message.chat.id))

@bot.message_handler(commands=['faq'])
def faq(message):
    send_menu(message.chat.id)

@bot.message_handler(content_types=['text'])
def detect_faq(message):
    chat_id = message.chat.id
    if message.text == faq_list[0]:
        bot.send_message(chat_id, faq_answer_list[0])
    elif message.text == faq_list[1]:
        bot.send_message(chat_id, faq_answer_list[1])
    elif message.text == faq_list[2]:
        bot.send_message(chat_id, faq_answer_list[2])
    elif message.text == "give me contacts":
        contacts = get_contacts_by_webhook()
        bot.send_message(chat_id, contacts)
        # print(bot.get_webhook_info())
    elif check_keywords(message.text, table) == []:
        pass
    else: 
        for answer in check_keywords(message.text, table):
            bot.send_message(chat_id, answer)

bot.polling(none_stop=True, interval=0) 


print('done')