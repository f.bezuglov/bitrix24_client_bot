import nltk
from nltk.corpus import stopwords
import pymorphy3
import re
import string
import pandas
import requests


nltk.download('stopwords')
stopwords = set(stopwords.words("russian"))
def is_stop_word(word):
    return word in stopwords

morph = pymorphy3.MorphAnalyzer(result_type=None)
def lemmatize(word):
    if morph.word_is_known(word):
        return morph.parse(word)[0][2]
    return word

regex = re.compile('([^\s\w]|_)+')
def remove_punct(value):
    bad_tokens = ["&#xD;", "«", "»"]
    bad_tokens.extend(list(string.punctuation))
    for bt in bad_tokens:
        value = value.replace(bt, "")
    value = regex.sub("", value)
    return value

def preprocess_text(value, del_punct=False, do_lemmatize=False, tokenizer=None, del_stop_words=False):
    tokenizer = tokenizer or (lambda s: s.split(" "))
    if del_punct:
        value = remove_punct(value)
    words = tokenizer(value) #nltk.word_tokenize(value, language="russian")
    if do_lemmatize:
        words = [lemmatize(w).lower() for w in words]
    if del_stop_words:
        words = [w for w in words if not is_stop_word(w)]
    for word in words:
        word = word.replace(' ', '')
        if word == '':
            words.remove(word)
    return " ".join(words)

def check_keywords(message, dataframe):
    message = preprocess_text(message, del_punct=True, do_lemmatize=True)
    faq_list = []
    for i, row in dataframe.iterrows():
        if dataframe.loc[i, 'Ключевое слово 1'] in message and dataframe.loc[i, 'Ключевое слово 2'] in message:
            faq_list.append(dataframe.loc[i, 'Ответ'])
    return faq_list




def get_contacts_by_webhook():
    webhook_endpoint = 'http://b24-5iy0e5.bitrix24.ru/rest/1/rkjw297oin8wvz53/'
    method = 'crm.contact.list.json'

    response = requests.get(webhook_endpoint + method)

    if response.status_code == 200:
        data = response.json()

        if 'result' in data:
            contacts = data['result']
            contacts_str_list = []
            for contact in contacts:
                contacts_str = str((f"Contact ID: {contact['ID']}, Name: {contact['NAME']}, Last Name: {contact['LAST_NAME']}"))
                contacts_str_list.append(contacts_str)
                return str(contacts_str_list)    
        else:
            print("No contacts found.")
            return "No contacts found."
    else:
        print(f"Error: {response.status_code} - {response.text}")
        return f"Error: {response.status_code} - {response.text}"