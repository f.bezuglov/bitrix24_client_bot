from telegram.ext import Updater, MessageHandler, CommandHandler, Filters, CallbackContext

# Replace "YOUR_TELEGRAM_BOT_TOKEN" with your actual bot token
updater = Updater("", use_context=True)

def start(update, context):
    update.message.reply_text('Hello! I am your Telegram bot.')

def handle_messages(update, context):
    # Extract the text message from the user
    user_message = update.message.text

    # Perform custom logic based on user's message
    if user_message == "1":
        response_text = "one"
    elif user_message == "2":
        response_text = "two"
    else:
        response_text = "I don't understand."

    # Send the response back to the user
    update.message.reply_text(response_text)

def main():
    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(MessageHandler(Filters.text & ~Filters.command, handle_messages))

    # Configure webhook settings
    updater.start_webhook(
        listen="0.0.0.0",  # Listen on all available interfaces
        port=8443,          # Port to use for the webhook (you can choose a different port)
        url_path="/my_webhook",  # Optional: Add a path to your webhook URL
    )
    
    # Set the webhook URL
    updater.bot.setWebhook(url="https://griborod.hhos.ru.s46.hhos.net/my_webhook")

    updater.idle()

if __name__ == '__main__':
    main()

print('done')