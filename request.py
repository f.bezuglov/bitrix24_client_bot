import requests

webhook_endpoint = 'http://b24-5iy0e5.bitrix24.ru/rest/1/xxx/'
method = 'crm.contact.list.json'

response = requests.get(webhook_endpoint + method)

if response.status_code == 200:
    data = response.json()

    if 'result' in data:
        contacts = data['result']
        for contact in contacts:
            contacts_str = str((f"Contact ID: {contact['ID']}, Name: {contact['NAME']}, Last Name: {contact['LAST_NAME']}"))
            print(contacts_str)
    else:
        print("No contacts found.")
else:
    print(f"Error: {response.status_code} - {response.text}")